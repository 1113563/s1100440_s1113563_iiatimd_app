package com.example.app;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MedicationList extends AppCompatActivity {
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.my_medication);

        ArrayList<Medication> medicationArrayList = new ArrayList<>();
        medicationArrayList.add(new Medication(R.drawable.ic_edit,"Line1"));

        mRecyclerView = findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new MedicationAdapter(medicationArrayList);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);


        //LOCALHOST API SHIT ARRAY
//        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest("https://drugbuddy.herokuapp.com/api/medicinelist",  new Response.Listener<JSONArray>() {
//            @Override
//            public void onResponse(JSONArray response) {
//
//                for (int i = 0 ; i < response.length(); i++) {
//                    try {
//                        ArrayList<String> meds = new ArrayList<String>();
//                        JSONObject jsonObject = response.getJSONObject(i);
//                        meds.add(jsonObject.toString());
//                        mAdapter = new MedicationAdapter(meds);
//
//                        Log.d("gelukt", jsonObject.toString());
//
//                    } catch (JSONException e) {
//                        Log.d("klote zooi", "kutandroid");
//                    }
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Log.d("gefaald", error.toString());
//            }
//        });
//
//        RequestQueue requestQueue = Volley.newRequestQueue(this);
//        requestQueue.add(jsonArrayRequest);
//
//        mRecyclerView.setAdapter(mAdapter);
    }
}
