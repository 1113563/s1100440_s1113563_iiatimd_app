package com.example.app;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LogIn extends AppCompatActivity {

    private EditText email;
    private EditText password;
    private Button btn_logIn;
    private String URL_LOGIN ="http://127.0.0.1:8000/api/login";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        btn_logIn = findViewById(R.id.logInButton);
        btn_logIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mEmail = email.getText().toString().trim();
                String mPassword = password.getText().toString().trim();

                if(!mEmail.isEmpty() || !mPassword.isEmpty()){
                    Login();
                } else {
                    email.setError("Please insert username");
                    password.setError("Please insert password");
                }
            }
        });
    }

    private void Login() {
        email = findViewById(R.id.newUserName);
        final String email = this.email.toString().trim();

        password = findViewById(R.id.newUserPassword);
        final String password = this.password.toString().trim();

        final Intent toDashBoardScreenIntent = new Intent(this,MainDashboard.class);
        final Intent toLoginScreenIntent = new Intent(this,LogIn.class);

        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_LOGIN+"?email="+email+"&password="+password, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    //go to dashboard
                    Log.d("gelukt", response);
                    JSONObject object = new JSONObject(response);
                        if(object.getBoolean("success")){
                            startActivity(toDashBoardScreenIntent);
                    } else {
                        Toast.makeText(LogIn.this, "email or password is wrong", Toast.LENGTH_SHORT).show();
                        startActivity(toLoginScreenIntent);
                    }
                }
                catch (Exception e){
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

}
