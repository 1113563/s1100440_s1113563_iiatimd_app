package com.example.app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.JsonRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button toLogInScreenBtn = findViewById(R.id.toLogIn);
        toLogInScreenBtn.setOnClickListener(this);

        Button toSignUpScreenBtn = findViewById(R.id.toSignUp);
        toSignUpScreenBtn.setOnClickListener(this);


    }

    public void onClick(View v){

        switch (v.getId()){
            case R.id.toLogIn:
                Intent toLogInScreenIntent = new Intent(this,LogIn.class);
                startActivity(toLogInScreenIntent);
                break;

            case R.id.toSignUp:
                Intent toSignUpScreenIntent = new Intent(this,SignUp.class);
                startActivity(toSignUpScreenIntent);
                break;
        }


    }


}
