package com.example.app;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

public class MainDashboard  extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_dashboard);

        Button toMedicationListButton = findViewById(R.id.medicationBtn);
        toMedicationListButton.setOnClickListener(this);

        Button toTimersButton = findViewById(R.id.timerButton);
        toTimersButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.medicationBtn:
                Intent toMedicationListIntent = new Intent(this,MedicationList.class);
                startActivity(toMedicationListIntent);
                break;
            case R.id.timerButton:
                Intent toTimerListIntent = new Intent(this, Timers.class);
                startActivity(toTimerListIntent);

        }


    }
}
