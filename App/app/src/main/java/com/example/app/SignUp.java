package com.example.app;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SignUp extends AppCompatActivity {
    private EditText name,email,password,c_password;
    private Button btn_SignUp;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sign_up);

        name = findViewById(R.id.newUserName);
        email = findViewById(R.id.userEmail);
        password = findViewById(R.id.newUserPassword);
        c_password = findViewById(R.id.confirmNewUserPassword);
        btn_SignUp = findViewById(R.id.signUpButton);

        btn_SignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SignUp();
            }
        });
    }
        private void SignUp(){
            btn_SignUp.setVisibility(View.GONE);

            final String name = this.name.getText().toString().trim();
            final String email = this.email.getText().toString().trim();
            final String password = this.password.getText().toString().trim();

            String URL_REGISTER ="https://drugbuddy.herokuapp.com/api/register?name="+ name +"&email="+email+"&password="+password;

            final Intent toLogInScreenIntent = new Intent(this,LogIn.class);

            StringRequest stringRequest = new StringRequest(Request.Method.POST, URL_REGISTER, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    JSONObject object = null;
                    try {
                        object = new JSONObject(response);
                        if (object.getBoolean("success")) {
                        } else {
                            Toast.makeText(SignUp.this, "email or password is wrong", Toast.LENGTH_SHORT).show();
                            startActivity(toLogInScreenIntent);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(SignUp.this, "Sign in error " + error.toString(), Toast.LENGTH_SHORT).show();
                    btn_SignUp.setVisibility(View.VISIBLE);
                    Log.d("kut", error.toString());
                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(stringRequest);
        }
    }
