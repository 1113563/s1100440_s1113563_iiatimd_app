package com.example.app;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MedicationAdapter extends RecyclerView.Adapter<MedicationAdapter.MedicationViewHolder> {
    private ArrayList<Medication> mMedicationArrayList;


    public MedicationAdapter(ArrayList<Medication> medicationArrayList){
        mMedicationArrayList = medicationArrayList;
    }

    public static class MedicationViewHolder extends RecyclerView.ViewHolder{
        public TextView textView;
        public ImageView imageView;

        public MedicationViewHolder(View v){
            super(v);
            textView = v.findViewById(R.id.textView);
            imageView = v.findViewById(R.id.imageView);
        }
    }

    @NonNull
    @Override
    public MedicationViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = (CardView) LayoutInflater.from(parent.getContext()).inflate(R.layout.medication_card, parent, false);
        MedicationViewHolder medicationViewHolder = new MedicationViewHolder(v);
        return medicationViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MedicationViewHolder holder, int position) {
        //holder.textView.setText([position]);
        Medication currentItem = mMedicationArrayList.get(position);
        holder.imageView.setImageResource(currentItem.getImageResource());
        holder.textView.setText(currentItem.getMedName());
    }

    @Override
    public int getItemCount() {
        return mMedicationArrayList.size();
    }

}
