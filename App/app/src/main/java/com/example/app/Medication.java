package com.example.app;

public class Medication {
    private int mImageResource;
    private String medName;

    public Medication(int imageResource, String name){
        mImageResource = imageResource;
        medName = name;
    }

    public int getImageResource(){
        return mImageResource;
    }

    public String getMedName(){
        return medName;
    }
}
